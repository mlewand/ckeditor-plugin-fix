/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * @fileOverview The "fix" plugin.
 *
 */
( function() {
	'use strict';

	// Define Quick Fix type.
	function ElementRemove( issue ) {
		this.issue = issue;
	}

	ElementRemove.prototype.fix = function( formAttributes, callback ) {
		this.issue.element.remove();

		if ( callback ) {
			callback( this );
		}
	};

	CKEDITOR.plugins.add( 'fix', {
		requires: 'a11ychecker',

		onLoad: function() {
			CKEDITOR.plugins.a11ychecker.on( 'quickFixesReady', function( repoEvent ) {
				// Just as Quick Fix repository is ready, we'll add a QF type.
				repoEvent.data.add( 'ListFix', ElementRemove );
			} );
		},

		init: function( editor ) {
			// We also need to register the Quick Fix to engine fixes mapping, to "teach"
			// Accessibility Checker that given issue type can be solved with our fix.
			editor._.a11ychecker.on( 'loaded', function() {
				editor._.a11ychecker.engine.fixesMapping.documentVisualListsAreMarkedUp = [ 'ListFix' ];
			} );
		}
	} );
} )();
