# ckeditor-plugin-fix

A sample plugin that illustrates how to add a custom Quick Fix to the [Accessibility Checker](http://cksource.com/ckeditor/services#accessibility-checker).

Note that this plugin requires Accessibility Checker core plugin in order to work correctly.

## Demo

Demo is available in `fix/dev/fix.html`.